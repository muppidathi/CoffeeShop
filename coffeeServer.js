const express = require("express");
const app = express();

app.use(express.json());
app.use(
  express.urlencoded({
    extended: true,
  }),
);

let shop = require('./routes/shop')
let db = require('./modules/dbConnection')
db.connectToServer(function(err) {
   console.log("db connection err",err)
});

app.get('/health',function(req,res){
  const response = { success : true }
  res.send(response);
  res.sendStatus('200');
  res.end();
})

app.get('/order_ready/:invoice_id',function(req,res){
  console.log("Order No "+req.params.invoice_id+" is ready to serve")
  res.send("Order No "+req.params.invoice_id+" is ready to serve");
  res.sendStatus('200');
  res.end();
})


app.use('/',shop);

app.listen(3000,function(){
  console.log("Coffee shop is open at 3000")
})

module.exports  =app;
