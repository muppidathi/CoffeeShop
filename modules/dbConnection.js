
const { MongoClient } = require('mongodb');
const fs = require('fs');

const credentials = 'C:/Users/muppidathi.murugan/Documents/My Documents/X509-cert-9179468332237504752.pem'

const client = new MongoClient('mongodb+srv://muppidathimurugan.jbnxs.mongodb.net/myFirstDatabase?authSource=%24external&authMechanism=MONGODB-X509&retryWrites=true&w=majority', {
  sslKey: credentials,
  sslCert: credentials
});


let dbConnection;

module.exports = {
  connectToServer: function (callback) {
    client.connect(function (err, db) {
      if (err || !db) {
        return callback(err);
      }

      dbConnection = db.db("coffee_shop");
      console.log("Successfully connected to MongoDB.");

      return callback();
    });
  },

  getDb: function () {
    return dbConnection;
  },
};