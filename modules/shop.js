const { ObjectId } = require('mongodb');
const db = require('./dbConnection')
const _ = require('underscore');

function getItems(input,callback){
  const dbConnect = db.getDb();
  let options = {};

  if(input.body.items){
    let items = _.map(input.body.items, function(item){ return new ObjectId(item); });
    options = {_id : { $in : items}}
  }

  dbConnect
  .collection("products")
  .find(options).limit(50).toArray(function (err, result) {
    if (err) {
      callback(err,null);
    } else {
      callback(null,result);
    }
  });
}

function getCategory(input,callback){
  const dbConnect = db.getDb();
  let options = {}

  if(input.length > 0){
    options = {
      $and : [{"_id" : { $in : input}}, {"is_offer_applicable" : 'yes'}]
    }
  }

  dbConnect
    .collection("category")
    .find(options).limit(50).toArray(function (err, result) {
      if (err) {
        callback(err,null);
      } else {
        callback(null,result);
      }
    });
}

function getOffers(callback){
  const dbConnect = db.getDb();

  dbConnect
    .collection("offers")
    .find({}).limit(50).toArray(function (err, result) {
      if (err) {
        callback(err,null);
      } else {
        callback(null,result);
      }
    });
}

function insertOrder(input,callback){
  const dbConnect = db.getDb();

  const matchDocument = {
    items: input.items,
    total: input.total,
    tax: input.tax,
    date: new Date()
  };

  dbConnect
    .collection("invoices")
    .insertOne(matchDocument, function (err, result) {
      if (err) {
        callback(err,null);
      } else {
        callback(null,result);
      }
    });
}



module.exports = {
  getItems : getItems,
  getCategory : getCategory,
  getOffers : getOffers,
  insertOrder : insertOrder
}
