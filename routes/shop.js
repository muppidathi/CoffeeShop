
const express = require("express");
const app = express.Router();
const shop = require('../modules/shop');
const async = require('async');
const _ = require('underscore');
const request = require('request')

app.get('/items',function(req,res){
  const response = {
    err : null,
    data : null
  }
  let statusCode = 200;
  shop.getItems(req,function(err,products) {
    if(err){
      response.err = err;
      statusCode = 503; // Service unavailable because of DB error
    }else{
      if(products.length == 0){
        response.err = 'Currently No Items Available !';
      }else{
        response.data = products;
      }
    }
    res.status(statusCode).send(response);
    res.end();
  })
});

app.get('/offers',function(req,res){
  const response = {
    err : null,
    data : null
  }
  let statusCode = 200;
  shop.getOffers(function(err,offers) {
    if(err){
      response.err = err;
      statusCode = 503; // Service unavailable because of DB error
    }else{
      if(offers.length == 0){
        response.err = 'Currently No Offers Available !';
      }else{
        response.data = offers;
      }
    }
    res.status(statusCode).send(response);
    res.end();
  })
});


app.get('/category',function(req,res){
  const response = {
    err : null,
    data : null
  }
  let statusCode = 200;
  shop.getCategory([],function(err,category) {
    if(err){
      response.err = err;
      statusCode = 503; // Service unavailable because of DB error
    }else{
      if(category.length == 0){
        response.err = 'Currently No Offers on category !';
      }else{
        response.data = category;
      }
    }
    res.status(statusCode).send(response);
    res.end();
  })
});

app.post('/order',function(req,res){
  const response = {
    err : null,
    data : null
  }
  let statusCode = 200;
  let isOffer = false;

  if(req.body.items == undefined || req.body.items == null || req.body.items.length == 0){
    response.err = 'INVALID ORDER';
    res.status(statusCode).send(response);
    res.end();
  }else{
    let orderedProducts = [];
    async.series([
      function(callback) {
        shop.getItems(req,function(err,products) {
          if(err){
            statusCode = 503; // Service unavailable because of DB error
            callback(err,null)
          }else{
            if(products.length == 0){
              callback('INVALID PRODUCT',null);
            }else{
              orderedProducts = products;
              callback(null,products)
            }
          }
        })
      },
      function(callback) {
        let categories = _.pluck(orderedProducts,'category')
        shop.getCategory(categories,function(err,category) {
          if(err){
            statusCode = 503; // Service unavailable because of DB error
            callback(err,null)
          }else{
            if(category.length > 0){
              isOffer = true;
            }
            callback(null,category)
          }
        })
      }
    ], function(err, results) {
      if(err){
        response.err = err;
        res.status(statusCode).send(response);
        res.end();
      }else{
        let calculatedTax = 0;
        let maximumAmount = 0;
        let data = {
          items : [],
          total : 0,
          tax : 0,
          discount : 0,
          amountToPay : 0,
          offerApplied : 'No Offer applied on your order'
        }

        _.map(orderedProducts,function(product) {
          data.total = parseInt(data.total) + parseInt(product.price);
          data.tax = parseInt(calculatedTax) + (product.price * product.tax/100);
        })

    
        if(orderedProducts.length >= 3){
          maximumAmount = _.max(orderedProducts, function(product){ return product.price; })
          if(isOffer){
            data.offerApplied = 'Your tax is zero and your maximum is reduced'
            data.discount = parseInt(data.tax) +  parseInt(maximumAmount.price);
            data.amountToPay = parseInt(data.total) - parseInt(maximumAmount.price);
          }else{
            data.discount =  parseInt(maximumAmount.price);
            data.amountToPay = parseInt(data.total) + parseInt(data.tax) - parseInt(maximumAmount.price);
          }
        }else if(isOffer){ 
          data.offerApplied = 'Your tax is zero'
          data.amountToPay = parseInt(data.total);
          data.discount =  parseInt(data.tax);
        }else{
          data.amountToPay = parseInt(data.total) + parseInt(data.tax);
          data.discount =  0;
        }

        data.items = _.map(orderedProducts,function (singleProduct) {
          return _.pick(singleProduct,['name','price'])
        })

        setTimeout(function(){
          request
          .get('http://localhost:3000/order_ready/2')
          .on('response', function(response) {
            console.log(response.statusCode) // 200
            console.log(response.headers['content-type']) // 'image/png'
          })
        }, 60000);

        response.data = data;
        res.status(statusCode).send(response);
        res.end();
      }
    });
  }
})

module.exports  = app;
